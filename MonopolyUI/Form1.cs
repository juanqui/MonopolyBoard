﻿using MonopoliyBusiness;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonopolyUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Object emp = new Object();
            //emp.Id = Convert.ToInt64(this.tbxId.Text);
            //emp.Name = this.tbxName.Text;

            EmployeeBusiness empBus = new EmployeeBusiness();
            empBus.Save(emp);

           

            this.LoadGrid();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.LoadGrid();

        }

        private void LoadGrid()
        {
            //EmployeeBusiness emp = new EmployeeBusiness();
            //this.dtgvwEmployees.DataSource = emp.LoadEmployee();
        }


        private void picBxParLibre_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("PARADA LIBRE", myFont, Brushes.Black, new Point(2, 20));
            }
        }

        private void picBxAvKentuly_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("AVENIDA ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("KENTUCKY", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$220", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxFortuna_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("FORTUNA", myFont, Brushes.Black, new Point(2, 20));
            }
        }

        private void picBoxIndiana_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("AVENIDA ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("INDIANA", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$220", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxAvIllinois_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("AVENIDA ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("ILINOIS", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$240", myFont, Brushes.Black, new Point(2, 40));
                e.Graphics.DrawString("ZZ", myFont, Brushes.Black, new Point(2, 50));
            }
        }

        private void picBxpicBxFerrocarril_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("FERROCARRIL ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("B & O", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$200", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxAvAtlantico_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("AVENIDA ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("ATLANTCO", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$260", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxVentnor_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("AVENIDA ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("VENTNOR", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$260", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxCompAgua_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("COMPAÑIA  ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("de AGUA", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$150", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxJardMarvin_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("JARDIN  ", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("MARVIN", myFont, Brushes.Black, new Point(2, 30));
                e.Graphics.DrawString("$280dd", myFont, Brushes.Black, new Point(2, 40));
            }
        }

        private void picBxParCarcel_Paint(object sender, PaintEventArgs e)
        {
            using (Font myFont = new Font("Arial", 9))
            {
                e.Graphics.DrawString("VAYASE A LA", myFont, Brushes.Black, new Point(2, 20));
                e.Graphics.DrawString("CARCEL", myFont, Brushes.Black, new Point(2, 30));
            }
        }
    }
}
